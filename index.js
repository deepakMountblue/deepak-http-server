const http = require('http')
const uuid = require('uuid')
const fs = require('fs')

const server = http.createServer((request, response) => {
  switch (request.url) {

    case "/":
      response.write("home")
      response.end()
      break;

    case "/html":
      fs.readFile('./index.html', (err, data) => {
        if (err) {
          response.writeHead(404)
          response.write("Html file not found")
        }
        else {
          response.writeHead(200, { 'Content-Type': "text/html" })
          response.write(data)
        }
        response.end()
      })
      break;

    case '/json':
      fs.readFile('./sample.json', 'utf-8', (err, data) => {
        if (err) {
          response.writeHead(404)
          response.write("Json file not found")
        }
        else {
          response.writeHead(200, { 'Content-Type': 'application/json' })
          response.write(JSON.stringify(JSON.parse(data)))
        }
        response.end()
      })

      break;

    case '/uuid':

      const id = uuid.v4()
      response.writeHead(200, { 'Content-Type': "application/json" })
      response.write(JSON.stringify({
        uuid: id
      }))
      response.end()
      break

    case '/status/' + request.url.split('/')[2]:
      const statusCode = request.url.split('/')[2]
      if (statusCode < 100 || statusCode > 599) {
        response.writeHead(404)
        response.write("Can't found status code " + statusCode)
      }
      else {
        response.writeHead(statusCode)
        response.write("check the status code in browser tools")
      }
      response.end()
      break

    case '/delay/' + request.url.split('/')[2]:

      response.writeHead(200)
      console.log("waiting for " + request.url.split('/')[2] + " seconds")
      setTimeout(() => {
        response.write("got after " + request.url.split('/')[2] + " seconds")
        response.end()
      }, request.url.split('/')[2] * 1000)
      break;

    default:

      response.writeHead(404)
      response.end()
      break;

  }

})

const PORT = 5000

server.listen(PORT, () => {
  console.log("listening to port 5000");
})